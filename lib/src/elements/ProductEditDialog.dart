import 'dart:io';

import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:image_picker/image_picker.dart';

import '../helpers/checkbox_form_field.dart';
import '../../generated/l10n.dart';
import '../controllers/category_controller.dart';
import '../models/product.dart';

class ProductEditDialog extends StatefulWidget {
  final Product product;
  final VoidCallback onChanged;

  ProductEditDialog({Key key, this.product, this.onChanged}) : super(key: key);

  @override
  _ProductEditDialogState createState() => _ProductEditDialogState();
}

class _ProductEditDialogState extends StateMVC<ProductEditDialog> {
  GlobalKey<FormState> _productsFormKey = new GlobalKey<FormState>();  

  final ImagePicker _picker = ImagePicker();
  PickedFile _imageFile;
  var currentSelected = null;

  CategoryController _con; 

  _ProductEditDialogState() : super(CategoryController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.listenForCategories();  
    super.initState();    
  }

  @override
  Widget build(BuildContext context) {    
    return FlatButton(        
      textColor: Colors.white,
      child: Icon(Icons.edit), 
      onPressed: () {
        showDialog(
            context: context,
            builder: (context) {
              return StatefulBuilder(
              builder: (context, setState) {
                return SimpleDialog(
                contentPadding: EdgeInsets.symmetric(horizontal: 20),
                titlePadding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                title: Row(
                  children: <Widget>[
                    Icon(Icons.inventory),
                    SizedBox(width: 10),
                    Text(
                      S.of(context).edit_product,
                      style: Theme.of(context).textTheme.bodyText1,
                    )
                  ],
                ),
                children: <Widget>[

                  Form(
                    key: _productsFormKey,
                    child: Column(
                      children: <Widget>[
                        new InkWell(
                          borderRadius: BorderRadius.circular(80),
                          child: CircleAvatar(
                            radius: 60.0,
                            backgroundImage: _imageFile != null 
                              ? FileImage(File(_imageFile.path)) 
                              : NetworkImage(widget.product.image.url),
                          ),
                          onTap: () async {                                                       
                            await showModalBottomSheet(
                              context: context, 
                              builder: ((builder) => menuImageWidget(context)),
                            ); 

                            setState(() {

                            });
                          },
                        ),
                        new TextFormField(  // NOMBRE DEL PRODUCTO FIELD
                          style: TextStyle(color: Theme.of(context).hintColor),
                          keyboardType: TextInputType.text,                          
                          decoration: getInputDecoration(hintText: S.of(context).productNameExample, labelText: S.of(context).productName),
                          initialValue: widget.product.name,
                          validator: (input) => input == '' ? S.of(context).productNameNotValid : null,
                          onSaved: (input) => widget.product.name = input,
                        ),
                        new TextFormField(  // PRECIO DEL PRODUCTO FIELD
                          style: TextStyle(color: Theme.of(context).hintColor),
                          keyboardType: TextInputType.number,
                          decoration: getInputDecoration(hintText: '0.0', labelText: S.of(context).productPrice),
                          initialValue: widget.product.price.toString(),
                          validator: (input) => input == '' ? S.of(context).productPriceNotValid : null,
                          onSaved: (input) => widget.product.price  = double.parse(input),
                        ),
                        new TextFormField(  // PRECIO DE DESCUENTO FIELD
                          style: TextStyle(color: Theme.of(context).hintColor),
                          keyboardType: TextInputType.number,                          
                          decoration: getInputDecoration(hintText: '0.0', labelText: S.of(context).productDiscount),
                          initialValue: widget.product.discountPrice.toString(),
                          //validator: (input) => input == '' ? null : null,
                          onSaved: (input) {                            
                            widget.product.discountPrice = input != '' ? double.parse(input) : null;  
                          } 
                        ),
                        new DropdownButtonFormField(
                          value: currentSelected = currentSelected != null ? currentSelected : widget.product.category.name,
                          onChanged: (newValue) {
                            setState(() {
                              currentSelected = newValue;
                            });
                          }, // CATEGORIA FIELD
                          style: TextStyle(color: Theme.of(context).hintColor),
                          decoration: getInputDecoration(hintText: 'Frutas', labelText: S.of(context).category),                          
                          items: _con.categorias.map((item) => DropdownMenuItem(child: Text(item.name), value: item.name)).toList(), 
                          validator: (input) => input == null ? S.of(context).select_category : null,
                          onSaved: (input) {
                            _con.categorias.forEach((categoria) {
                                if (categoria.name == input){
                                  widget.product.category = categoria;                            
                                }                                
                             });                            
                          }, 
                        ),
                        new TextFormField(  // DESCRIPCION DEL PRODUCTO
                          style: TextStyle(color: Theme.of(context).hintColor),
                          keyboardType: TextInputType.text,                          
                          decoration: getInputDecoration(hintText: 'Breve descripción del producto.', labelText: S.of(context).productDescription),
                          initialValue: widget.product.description?.replaceAll("<p>", "")?.replaceAll("</p>", ""),
                          //validator: (input) => input.trim().length < 3 ? S.of(context).not_a_valid_phone : null,
                          onSaved: (input) => widget.product.description = input,
                        ),
                        new TextFormField(  // CAPACITY
                          style: TextStyle(color: Theme.of(context).hintColor),
                          keyboardType: TextInputType.number,                          
                          decoration: getInputDecoration(hintText: '0.0', labelText: S.of(context).productCapacity),
                          initialValue: widget.product.capacity?.replaceAll("null", ""),
                          //validator: (input) => input.trim().length < 3 ? S.of(context).not_a_valid_phone : null,
                          onSaved: (input) => widget.product.capacity = input.toString(),
                        ),
                        new TextFormField(  // UNIDAD
                          style: TextStyle(color: Theme.of(context).hintColor),
                          keyboardType: TextInputType.text,                          
                          decoration: getInputDecoration(hintText: 'Caja, Lb, Kg, g', labelText: S.of(context).productUnit),
                          initialValue: widget.product.unit,
                          //validator: (input) => input.trim().length < 3 ? S.of(context).not_a_valid_phone : null,
                          onSaved: (input) => widget.product.unit = input,
                        ),
                        new TextFormField(  // PACKAGE ITEMS COUNT
                          style: TextStyle(color: Theme.of(context).hintColor),
                          keyboardType: TextInputType.number,                          
                          decoration: getInputDecoration(hintText: '0', labelText: S.of(context).productPackageItems),
                          initialValue: widget.product.packageItemsCount?.replaceAll("null", ""),
                          //validator: (input) => input.trim().length < 3 ? S.of(context).not_a_valid_phone : null,
                          onSaved: (input) => widget.product.packageItemsCount = input.toString(),
                        ),
                        new CheckboxFormField(
                            title: Text(S.of(context).productDeliverable,
                                    style: TextStyle(color: Theme.of(context).hintColor, fontSize: 13),),
                            onSaved: (input) => widget.product.deliverable = input, context: context,
                            initialValue: widget.product.deliverable,                            
                        ),
                        new CheckboxFormField(
                            title: Text(S.of(context).available,
                                    style: TextStyle(color: Theme.of(context).hintColor, fontSize: 13),),
                            onSaved: (input) => widget.product.featured = input, context: context,
                            initialValue: widget.product.featured,                            
                        ),
                      ],
                    ),
                  ),

                  SizedBox(height: 20),
                  Row(
                    children: <Widget>[
                      MaterialButton(
                        onPressed: () {                         
                          _imageFile = null;
                          Navigator.pop(context);                          
                        },
                        child: Text(S.of(context).cancel),
                      ),
                      MaterialButton(
                        onPressed: _submit,
                        child: Text(
                          S.of(context).edit,
                          style: TextStyle(color: Theme.of(context).accentColor),
                        ),
                      ),
                    ],
                    mainAxisAlignment: MainAxisAlignment.end,
                  ),
                  SizedBox(height: 10),
                ],
              );
              });
          });              
      },     
    );
  } 

  InputDecoration getInputDecoration({String hintText, String labelText}) {
    return new InputDecoration(
      hintText: hintText,
      labelText: labelText,
      hintStyle: Theme.of(context).textTheme.bodyText2.merge(
            TextStyle(color: Theme.of(context).focusColor),
          ),
      enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).hintColor.withOpacity(0.2))),
      focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).hintColor)),
      floatingLabelBehavior: FloatingLabelBehavior.auto,
      labelStyle: Theme.of(context).textTheme.bodyText2.merge(
            TextStyle(color: Theme.of(context).hintColor),
          ),
    );
  }

  void _submit() {       
    if (_productsFormKey.currentState.validate()) {      
      _imageFile = null;
      _productsFormKey.currentState.save();   
      widget.onChanged();
      Navigator.pop(context);
    }
  }

  void takePicture(ImageSource source) async {
    final pickFile = await _picker.getImage(
      source: source
    );
    _imageFile = pickFile;        
  }
  

  Widget menuImageWidget(BuildContext context) {
    return Container(
      height: 140.0,
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 20,
      ),
      child: Column(
        children: <Widget>[
          Text(
            S.of(context).choosePhoto,
            style: TextStyle(color: Theme.of(context).hintColor),
          ),
          SizedBox(
            height: 20,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                width: MediaQuery.of(context).size.width,
                child: FlatButton.icon(
                  onPressed: () async{                                        
                    await takePicture(ImageSource.camera);  
                    Navigator.of(context).pop();  
                    setState(() {});
                  }, 
                  icon: Icon(Icons.camera_alt), 
                  label: Text(S.of(context).camera),
                ),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width,
                child: FlatButton.icon(
                  onPressed: () async{
                    await takePicture(ImageSource.gallery);  
                    Navigator.of(context).pop();
                    setState(() {});
                  }, 
                  icon: Icon(Icons.image), 
                  label: Text(S.of(context).gallery)
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
