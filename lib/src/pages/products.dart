import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../controllers/market_controller.dart';
import '../elements/EmptyMarketsWidget.dart';
import '../elements/ProductDataDialog.dart';
import '../elements/ProductItemWidget.dart';
import '../models/product.dart';
import '../models/media.dart';

class ProductsWidget extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  ProductsWidget({Key key, this.parentScaffoldKey}) : super(key: key);

  @override
  _ProductsWidgetState createState() => _ProductsWidgetState();
}

class _ProductsWidgetState extends StateMVC<ProductsWidget> {
  MarketController _con;
  Product new_product = new Product();

  _ProductsWidgetState() : super(MarketController()) {
    _con = controller;    
  }

  @override
  void initState() {    
    _con.listenForProductMarkets();         
    super.initState();
  }

  @override
  Widget build(BuildContext context) {    

    return Scaffold(
      key: _con.scaffoldKey,
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(Icons.sort, color: Theme.of(context).hintColor),
          onPressed: () => widget.parentScaffoldKey.currentState.openDrawer(),
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          S.of(context).myProducts,
          style: Theme.of(context).textTheme.headline6.merge(TextStyle(letterSpacing: 1.3)),
        ),        
      ),
      body: RefreshIndicator(
          onRefresh: _con.refreshMarkets,
          child: _con.products.isEmpty
            ? EmptyMarketsWidget()
            : ListView.builder(
              shrinkWrap: true,
              primary: true,              
              itemCount: _con.products.length,
              itemBuilder: (context, index) {
                return ProductItemWidget(
                    heroTag: 'details_featured_product',
                    product: _con.products.elementAt(index),
                    cox: _con,
                  );
              },
            )
      ),
      floatingActionButton: FloatingActionButton(
        //backgroundColor: Theme.of(context).accentColor,
        onPressed: () { },
        child: ButtonTheme(
          child: ProductDataDialog(  
            product: new_product,
            onChanged: () {                     
              new_product.market = _con.markets.first;                        
              new_product.featured = true;              

              _con.addProduct(new_product);     
              _con.scaffoldKey.currentState.build(context);              
              _con.refreshMarkets();  
            },
          ),
        ),  
      ),
    );  
  }
}

