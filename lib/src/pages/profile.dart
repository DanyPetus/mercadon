import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../controllers/profile_controller.dart';
import '../elements/CircularLoadingWidget.dart';
import '../elements/ProfileAvatarWidget.dart';
import '../elements/ShoppingCartButtonWidget.dart';
import '../elements/StatisticsCarouselWidget.dart';

import '../repository/user_repository.dart';
import '../models/user.dart';
import 'package:image_picker/image_picker.dart';


class ProfileWidget extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  ProfileWidget({Key key, this.parentScaffoldKey}) : super(key: key);

  @override
  _ProfileWidgetState createState() => _ProfileWidgetState();
}

class _ProfileWidgetState extends StateMVC<ProfileWidget> {
  User user = new User();
  ProfileController _con;

  _ProfileWidgetState() : super(ProfileController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.listenForStatistics();
    super.initState();
    listenForUser();
  }

  void listenForUser() {
    getCurrentUser().then((_user) {
      setState(() {
        user = _user;
        print("usuario $user");
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(Icons.sort, color: Theme.of(context).primaryColor),
          onPressed: () => widget.parentScaffoldKey?.currentState?.openDrawer(),
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Theme.of(context).accentColor,
        elevation: 0,
        centerTitle: true,
        title: Text(
          S.of(context).profile,
          style: Theme.of(context).textTheme.headline6.merge(TextStyle(letterSpacing: 1.3, color: Theme.of(context).primaryColor)),
        ),
        actions: <Widget>[
          new ShoppingCartButtonWidget(iconColor: Theme.of(context).primaryColor, labelColor: Theme.of(context).hintColor),
        ],
      ),
      key: _con.scaffoldKey,
      body: _con.user.apiToken == null
          ? CircularLoadingWidget(height: 500)
          : SingleChildScrollView(
//              padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 10),
              child: Column(
                children: <Widget>[
                  ProfileAvatarWidget(user: _con.user),
                  StatisticsCarouselWidget(statisticsList: _con.statistics),
                  ListTile(
                    contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    leading: Icon(
                      Icons.person,
                      color: Theme.of(context).hintColor,
                    ),
                    title: Text(
                      S.of(context).about,
                      style: Theme.of(context).textTheme.headline4,
                    ),
                  ),
                  Column(
                    children: <Widget>[
                      Text(
                       "Nombre de usuario",
                        style: Theme.of(context).textTheme.headline2,
                      ),
                      Text(
                        _con.user.name,
                        style: Theme.of(context).textTheme.headline3,
                      ),
                      Text(
                        "Dirección registrada",
                        style: Theme.of(context).textTheme.headline2,
                      ),
                      Text(
                        _con.user.address,
                        style: Theme.of(context).textTheme.headline3,
                      ),
                      Text(
                        "Biografía",
                        style: Theme.of(context).textTheme.headline2,
                      ),
                      Text(
                        _con.user.bio,
                        style: Theme.of(context).textTheme.headline3,
                      ),
                      Text(
                        "Teléfono registrado",
                        style: Theme.of(context).textTheme.headline2,
                      ),
                      Text(
                        _con.user.phone,
                        style: Theme.of(context).textTheme.headline3,
                      ),
                      Text(
                        "Correo eléctronico",
                        style: Theme.of(context).textTheme.headline2,
                      ),
                      Text(
                        _con.user.email,
                        style: Theme.of(context).textTheme.headline3,
                      )
                    ],
                  ),
                 // Padding(
                   // padding: const EdgeInsets.symmetric(horizontal: 20),
                    //child: Text(
                     // _con.user.bio,
                      //style: Theme.of(context).textTheme.bodyText2,
                    //),
                  //),
                ],
              ),
            ),
    );
  }
}
