
import 'dart:async';

import 'package:firebase_messaging/firebase_messaging.dart';

class PushNotificationService {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  final _mensajeStreamController = StreamController<String>.broadcast();
  Stream<String> get mensajesStream => _mensajeStreamController.stream;

  static Future<dynamic> onBackgroundMessage(Map<String, dynamic> message) async {   

    if (message.containsKey('data')) {
      // Handle data message
      final dynamic data = message['data'];
    }

    if (message.containsKey('notification')) {
      // Handle notification message
      final dynamic notification = message['notification'];
    }         
  }

  initNotifications() async {
      await _firebaseMessaging.requestNotificationPermissions();
      final token = await _firebaseMessaging.getToken();

      print('-----------------------------------------------------------------------------');
      print('token: $token');
      print('-----------------------------------------------------------------------------');

      _firebaseMessaging.configure(
        onMessage: onMessage,
        onBackgroundMessage: onBackgroundMessage,
        onLaunch: onLaunch,
        onResume: onResume        
      );
    }  

  Future<dynamic> onMessage(Map<String, dynamic> message) async {
    _mensajeStreamController.sink.add( message['data'] );
  }  

  Future<dynamic> onResume(Map<String, dynamic> message) async {
    _mensajeStreamController.sink.add( message['data'] );
  }

  Future<dynamic> onLaunch(Map<String, dynamic> message) async {
    _mensajeStreamController.sink.add( message['data'] );
  }

  dispose() {
    _mensajeStreamController?.close();
  }
}