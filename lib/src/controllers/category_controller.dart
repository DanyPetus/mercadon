import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../models/category.dart';
import '../repository/category_repository.dart';

class CategoryController extends ControllerMVC {
  List<Category> categorias = <Category>[];
  GlobalKey<ScaffoldState> scaffoldKey;

  CategoryController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  void listenForCategories({String message}) async {
    final Stream<Category> stream = await getCategories();
    stream.listen((Category _category) {
      setState(() => categorias.add(_category));
    }, onError: (a) {
      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text(S.of(context).verify_your_internet_connection),
      ));
    }, onDone: () {
      if(message != null) {
        scaffoldKey?.currentState?.showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
    });
  }
}
