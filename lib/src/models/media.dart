import 'dart:convert';
import 'dart:io';

import 'package:path/path.dart';
import 'package:mime_type/mime_type.dart';
import 'package:global_configuration/global_configuration.dart';
import '../models/product.dart';
import 'package:uuid/uuid.dart';

class Media {
  String id;
  String name;
  String url;
  String thumb;
  String icon;
  String size;
  String model_id;
  String model_type;
  String collection_name;

  File file;

  Media() {
    url = "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
    thumb = "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
    icon = "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
  }

  Media.fromJSON(Map<String, dynamic> jsonMap) {
    try {            
      id = jsonMap['id'].toString();
      name = jsonMap['name'];
      url = jsonMap['url'];
      thumb = jsonMap['thumb'];
      icon = jsonMap['icon'];
      size = jsonMap['formated_size'];
    } catch (e) {
      url = "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
      thumb = "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
      icon = "${GlobalConfiguration().getValue('base_url')}images/image_default.png";
      print(e);
    }
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    var uuid = Uuid();

    map["id"] = id;
    map["name"] = name;
    map["url"] = url;
    map["thumb"] = thumb;
    map["icon"] = icon;
    map["formated_size"] = size;
    map["model_type"] = model_type;
    map["collection_name"] = collection_name;   
    try{
    map["mime"] = extensionFromMime(file.path);
    map["file"] = base64Encode(file.readAsBytesSync());;
    map["uuid"] = uuid.v4();
    map["file_name"] = basename(file.path);
    map["user_id"] = "1";
    } catch(error){};
    return map;
  }
    
    Map ofProductToMap(Product product) {
      var map = this.toMap();
      map["model_id"] = product.id;
      return map;
    }
  
    @override
    String toString() {
      return this.toMap().toString();
    }  
}
