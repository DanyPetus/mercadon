// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values

class S {
  S();
  
  static S current;
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name); 
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      S.current = S();
      
      return S.current;
    });
  } 

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Iniciar sesión`
  String get login {
    return Intl.message(
      'Iniciar sesión',
      name: 'login',
      desc: '',
      args: [],
    );
  }

  /// `Omitir`
  String get skip {
    return Intl.message(
      'Omitir',
      name: 'skip',
      desc: '',
      args: [],
    );
  }

  /// `Acerca de`
  String get about {
    return Intl.message(
      'Acerca de',
      name: 'about',
      desc: '',
      args: [],
    );
  }

  /// `Enviar`
  String get submit {
    return Intl.message(
      'Enviar',
      name: 'submit',
      desc: '',
      args: [],
    );
  }

  /// `Verificar`
  String get verify {
    return Intl.message(
      'Verificar',
      name: 'verify',
      desc: '',
      args: [],
    );
  }

  /// `Selecciona tus idiomas preferidos`
  String get select_your_preferred_languages {
    return Intl.message(
      'Selecciona tus idiomas preferidos',
      name: 'select_your_preferred_languages',
      desc: '',
      args: [],
    );
  }

  /// `Solicitar ID`
  String get order_id {
    return Intl.message(
      'Solicitar ID',
      name: 'order_id',
      desc: '',
      args: [],
    );
  }

  /// `Categoría`
  String get category {
    return Intl.message(
      'Categoría',
      name: 'category',
      desc: '',
      args: [],
    );
  }

  /// `Revisa`
  String get checkout {
    return Intl.message(
      'Revisa',
      name: 'checkout',
      desc: '',
      args: [],
    );
  }

  /// `Modo de pago`
  String get payment_mode {
    return Intl.message(
      'Modo de pago',
      name: 'payment_mode',
      desc: '',
      args: [],
    );
  }

  /// `Total parcial`
  String get subtotal {
    return Intl.message(
      'Total parcial',
      name: 'subtotal',
      desc: '',
      args: [],
    );
  }

  /// `Total`
  String get total {
    return Intl.message(
      'Total',
      name: 'total',
      desc: '',
      args: [],
    );
  }

  /// `Comidas favoritas`
  String get favorite_products {
    return Intl.message(
      'Comidas favoritas',
      name: 'favorite_products',
      desc: '',
      args: [],
    );
  }

  /// `sol`
  String get g {
    return Intl.message(
      'sol',
      name: 'g',
      desc: '',
      args: [],
    );
  }

  /// `Extras`
  String get extras {
    return Intl.message(
      'Extras',
      name: 'extras',
      desc: '',
      args: [],
    );
  }

  /// `Preguntas más frecuentes`
  String get faq {
    return Intl.message(
      'Preguntas más frecuentes',
      name: 'faq',
      desc: '',
      args: [],
    );
  }

  /// `Ayuda y apoyos`
  String get help_supports {
    return Intl.message(
      'Ayuda y apoyos',
      name: 'help_supports',
      desc: '',
      args: [],
    );
  }

  /// `Idioma de la aplicación`
  String get app_language {
    return Intl.message(
      'Idioma de la aplicación',
      name: 'app_language',
      desc: '',
      args: [],
    );
  }

  /// `¿Olvidé la contraseña?`
  String get i_forgot_password {
    return Intl.message(
      '¿Olvidé la contraseña?',
      name: 'i_forgot_password',
      desc: '',
      args: [],
    );
  }

  /// `No tengo una cuenta`
  String get i_dont_have_an_account {
    return Intl.message(
      'No tengo una cuenta',
      name: 'i_dont_have_an_account',
      desc: '',
      args: [],
    );
  }

  /// `Notificaciones`
  String get notifications {
    return Intl.message(
      'Notificaciones',
      name: 'notifications',
      desc: '',
      args: [],
    );
  }

  /// `Imp`
  String get tax {
    return Intl.message(
      'Imp',
      name: 'tax',
      desc: '',
      args: [],
    );
  }

  /// `Perfil`
  String get profile {
    return Intl.message(
      'Perfil',
      name: 'profile',
      desc: '',
      args: [],
    );
  }

  /// `Casa`
  String get home {
    return Intl.message(
      'Casa',
      name: 'home',
      desc: '',
      args: [],
    );
  }

  /// `Contra reembolso`
  String get cash_on_delivery {
    return Intl.message(
      'Contra reembolso',
      name: 'cash_on_delivery',
      desc: '',
      args: [],
    );
  }

  /// `Órdenes recientes`
  String get recent_orders {
    return Intl.message(
      'Órdenes recientes',
      name: 'recent_orders',
      desc: '',
      args: [],
    );
  }

  /// `Configuraciones`
  String get settings {
    return Intl.message(
      'Configuraciones',
      name: 'settings',
      desc: '',
      args: [],
    );
  }

  /// `Configuración de perfil`
  String get profile_settings {
    return Intl.message(
      'Configuración de perfil',
      name: 'profile_settings',
      desc: '',
      args: [],
    );
  }

  /// `Nombre completo`
  String get full_name {
    return Intl.message(
      'Nombre completo',
      name: 'full_name',
      desc: '',
      args: [],
    );
  }

  /// `Correo electrónico`
  String get email {
    return Intl.message(
      'Correo electrónico',
      name: 'email',
      desc: '',
      args: [],
    );
  }

  /// `Teléfono`
  String get phone {
    return Intl.message(
      'Teléfono',
      name: 'phone',
      desc: '',
      args: [],
    );
  }

  /// `Habla a`
  String get address {
    return Intl.message(
      'Habla a',
      name: 'address',
      desc: '',
      args: [],
    );
  }

  /// `Ajustes de Aplicacion`
  String get app_settings {
    return Intl.message(
      'Ajustes de Aplicacion',
      name: 'app_settings',
      desc: '',
      args: [],
    );
  }

  /// `Idiomas`
  String get languages {
    return Intl.message(
      'Idiomas',
      name: 'languages',
      desc: '',
      args: [],
    );
  }

  /// `Español`
  String get english {
    return Intl.message(
      'Español',
      name: 'english',
      desc: '',
      args: [],
    );
  }

  /// `Servicio de asistencia`
  String get help_support {
    return Intl.message(
      'Servicio de asistencia',
      name: 'help_support',
      desc: '',
      args: [],
    );
  }

  /// `Registrarse`
  String get register {
    return Intl.message(
      'Registrarse',
      name: 'register',
      desc: '',
      args: [],
    );
  }

  /// `¡Comencemos con el registro!`
  String get lets_start_with_register {
    return Intl.message(
      '¡Comencemos con el registro!',
      name: 'lets_start_with_register',
      desc: '',
      args: [],
    );
  }

  /// `Debe tener más de 3 letras.`
  String get should_be_more_than_3_letters {
    return Intl.message(
      'Debe tener más de 3 letras.',
      name: 'should_be_more_than_3_letters',
      desc: '',
      args: [],
    );
  }

  /// `John Doe`
  String get john_doe {
    return Intl.message(
      'John Doe',
      name: 'john_doe',
      desc: '',
      args: [],
    );
  }

  /// `Debería ser un correo electrónico válido`
  String get should_be_a_valid_email {
    return Intl.message(
      'Debería ser un correo electrónico válido',
      name: 'should_be_a_valid_email',
      desc: '',
      args: [],
    );
  }

  /// `Debe tener más de 6 letras.`
  String get should_be_more_than_6_letters {
    return Intl.message(
      'Debe tener más de 6 letras.',
      name: 'should_be_more_than_6_letters',
      desc: '',
      args: [],
    );
  }

  /// `Contraseña`
  String get password {
    return Intl.message(
      'Contraseña',
      name: 'password',
      desc: '',
      args: [],
    );
  }

  /// `Tengo cuenta? Atrás para iniciar sesión`
  String get i_have_account_back_to_login {
    return Intl.message(
      'Tengo cuenta? Atrás para iniciar sesión',
      name: 'i_have_account_back_to_login',
      desc: '',
      args: [],
    );
  }

  /// `Orden de seguimiento`
  String get tracking_order {
    return Intl.message(
      'Orden de seguimiento',
      name: 'tracking_order',
      desc: '',
      args: [],
    );
  }

  /// `Descubrir y explorar`
  String get discover__explorer {
    return Intl.message(
      'Descubrir y explorar',
      name: 'discover__explorer',
      desc: '',
      args: [],
    );
  }

  /// `Puede descubrir marketes y comida rápida a su alrededor y elegir su mejor comida después de unos minutos, la preparamos y se la entregamos.`
  String get you_can_discover_markets {
    return Intl.message(
      'Puede descubrir marketes y comida rápida a su alrededor y elegir su mejor comida después de unos minutos, la preparamos y se la entregamos.',
      name: 'you_can_discover_markets',
      desc: '',
      args: [],
    );
  }

  /// `Restablecer carro?`
  String get reset_cart {
    return Intl.message(
      'Restablecer carro?',
      name: 'reset_cart',
      desc: '',
      args: [],
    );
  }

  /// `Carro`
  String get cart {
    return Intl.message(
      'Carro',
      name: 'cart',
      desc: '',
      args: [],
    );
  }

  /// `Carrito de compras`
  String get shopping_cart {
    return Intl.message(
      'Carrito de compras',
      name: 'shopping_cart',
      desc: '',
      args: [],
    );
  }

  /// `Verifique su cantidad y haga clic en pagar`
  String get verify_your_quantity_and_click_checkout {
    return Intl.message(
      'Verifique su cantidad y haga clic en pagar',
      name: 'verify_your_quantity_and_click_checkout',
      desc: '',
      args: [],
    );
  }

  /// `¡Comencemos con el inicio de sesión!`
  String get lets_start_with_login {
    return Intl.message(
      '¡Comencemos con el inicio de sesión!',
      name: 'lets_start_with_login',
      desc: '',
      args: [],
    );
  }

  /// `Debe tener más de 3 caracteres.`
  String get should_be_more_than_3_characters {
    return Intl.message(
      'Debe tener más de 3 caracteres.',
      name: 'should_be_more_than_3_characters',
      desc: '',
      args: [],
    );
  }

  /// `Debe agregar alimentos de los mismos marketes, elija uno solo.`
  String get you_must_add_products_of_the_same_markets_choose_one {
    return Intl.message(
      'Debe agregar alimentos de los mismos marketes, elija uno solo.',
      name: 'you_must_add_products_of_the_same_markets_choose_one',
      desc: '',
      args: [],
    );
  }

  /// `Restablezca su carrito y ordene comidas de este markete`
  String get reset_your_cart_and_order_meals_form_this_market {
    return Intl.message(
      'Restablezca su carrito y ordene comidas de este markete',
      name: 'reset_your_cart_and_order_meals_form_this_market',
      desc: '',
      args: [],
    );
  }

  /// `Mantenga sus comidas antiguas de este markete.`
  String get keep_your_old_meals_of_this_market {
    return Intl.message(
      'Mantenga sus comidas antiguas de este markete.',
      name: 'keep_your_old_meals_of_this_market',
      desc: '',
      args: [],
    );
  }

  /// `Reiniciar`
  String get reset {
    return Intl.message(
      'Reiniciar',
      name: 'reset',
      desc: '',
      args: [],
    );
  }

  /// `Cerrar`
  String get close {
    return Intl.message(
      'Cerrar',
      name: 'close',
      desc: '',
      args: [],
    );
  }

  /// `Preferencias de aplicación`
  String get application_preferences {
    return Intl.message(
      'Preferencias de aplicación',
      name: 'application_preferences',
      desc: '',
      args: [],
    );
  }

  /// `Servicio de asistencia`
  String get help__support {
    return Intl.message(
      'Servicio de asistencia',
      name: 'help__support',
      desc: '',
      args: [],
    );
  }

  /// `Modo de luz`
  String get light_mode {
    return Intl.message(
      'Modo de luz',
      name: 'light_mode',
      desc: '',
      args: [],
    );
  }

  /// `Modo oscuro`
  String get dark_mode {
    return Intl.message(
      'Modo oscuro',
      name: 'dark_mode',
      desc: '',
      args: [],
    );
  }

  /// `Cerrar sesión`
  String get log_out {
    return Intl.message(
      'Cerrar sesión',
      name: 'log_out',
      desc: '',
      args: [],
    );
  }

  /// `Versión`
  String get version {
    return Intl.message(
      'Versión',
      name: 'version',
      desc: '',
      args: [],
    );
  }

  /// `No tengo ningún artículo en tu carrito`
  String get dont_have_any_item_in_your_cart {
    return Intl.message(
      'No tengo ningún artículo en tu carrito',
      name: 'dont_have_any_item_in_your_cart',
      desc: '',
      args: [],
    );
  }

  /// `Empieza a explorar`
  String get start_exploring {
    return Intl.message(
      'Empieza a explorar',
      name: 'start_exploring',
      desc: '',
      args: [],
    );
  }

  /// `No tengo ningún elemento en la lista de notificaciones`
  String get dont_have_any_item_in_the_notification_list {
    return Intl.message(
      'No tengo ningún elemento en la lista de notificaciones',
      name: 'dont_have_any_item_in_the_notification_list',
      desc: '',
      args: [],
    );
  }

  /// `Configuraciones de pago`
  String get payment_settings {
    return Intl.message(
      'Configuraciones de pago',
      name: 'payment_settings',
      desc: '',
      args: [],
    );
  }

  /// `No es un numero valido`
  String get not_a_valid_number {
    return Intl.message(
      'No es un numero valido',
      name: 'not_a_valid_number',
      desc: '',
      args: [],
    );
  }

  /// `No es una fecha valida`
  String get not_a_valid_date {
    return Intl.message(
      'No es una fecha valida',
      name: 'not_a_valid_date',
      desc: '',
      args: [],
    );
  }

  /// `No es un CVC válido`
  String get not_a_valid_cvc {
    return Intl.message(
      'No es un CVC válido',
      name: 'not_a_valid_cvc',
      desc: '',
      args: [],
    );
  }

  /// `Cancelar`
  String get cancel {
    return Intl.message(
      'Cancelar',
      name: 'cancel',
      desc: '',
      args: [],
    );
  }

  /// `Guardar`
  String get save {
    return Intl.message(
      'Guardar',
      name: 'save',
      desc: '',
      args: [],
    );
  }

  /// `Editar`
  String get edit {
    return Intl.message(
      'Editar',
      name: 'edit',
      desc: '',
      args: [],
    );
  }

  /// `No es un nombre completo válido`
  String get not_a_valid_full_name {
    return Intl.message(
      'No es un nombre completo válido',
      name: 'not_a_valid_full_name',
      desc: '',
      args: [],
    );
  }

  /// `Dirección de correo electrónico`
  String get email_address {
    return Intl.message(
      'Dirección de correo electrónico',
      name: 'email_address',
      desc: '',
      args: [],
    );
  }

  /// `No es un correo valido`
  String get not_a_valid_email {
    return Intl.message(
      'No es un correo valido',
      name: 'not_a_valid_email',
      desc: '',
      args: [],
    );
  }

  /// `No es un teléfono válido`
  String get not_a_valid_phone {
    return Intl.message(
      'No es un teléfono válido',
      name: 'not_a_valid_phone',
      desc: '',
      args: [],
    );
  }

  /// `No es una direccion valida`
  String get not_a_valid_address {
    return Intl.message(
      'No es una direccion valida',
      name: 'not_a_valid_address',
      desc: '',
      args: [],
    );
  }

  /// `No es una biografía válida.`
  String get not_a_valid_biography {
    return Intl.message(
      'No es una biografía válida.',
      name: 'not_a_valid_biography',
      desc: '',
      args: [],
    );
  }

  /// `Tu biografía`
  String get your_biography {
    return Intl.message(
      'Tu biografía',
      name: 'your_biography',
      desc: '',
      args: [],
    );
  }

  /// `Su dirección`
  String get your_address {
    return Intl.message(
      'Su dirección',
      name: 'your_address',
      desc: '',
      args: [],
    );
  }

  /// `Buscar`
  String get search {
    return Intl.message(
      'Buscar',
      name: 'search',
      desc: '',
      args: [],
    );
  }

  /// `Busquedas recientes`
  String get recents_search {
    return Intl.message(
      'Busquedas recientes',
      name: 'recents_search',
      desc: '',
      args: [],
    );
  }

  /// `Verifica tu conexión a internet`
  String get verify_your_internet_connection {
    return Intl.message(
      'Verifica tu conexión a internet',
      name: 'verify_your_internet_connection',
      desc: '',
      args: [],
    );
  }

  /// `No tienes ordenes`
  String get you_dont_have_any_order {
    return Intl.message(
      'No tienes ordenes',
      name: 'you_dont_have_any_order',
      desc: '',
      args: [],
    );
  }

  /// `Carros actualizados con éxito`
  String get carts_refreshed_successfuly {
    return Intl.message(
      'Carros actualizados con éxito',
      name: 'carts_refreshed_successfuly',
      desc: '',
      args: [],
    );
  }

  /// `El $productName se eliminó de su carrito`
  String get the_product_was_removed_from_your_cart {
    return Intl.message(
      'El \$productName se eliminó de su carrito',
      name: 'the_product_was_removed_from_your_cart',
      desc: '',
      args: [],
    );
  }

  /// `Categoría actualizada correctamente`
  String get category_refreshed_successfuly {
    return Intl.message(
      'Categoría actualizada correctamente',
      name: 'category_refreshed_successfuly',
      desc: '',
      args: [],
    );
  }

  /// `Notificaciones actualizadas con éxito`
  String get notifications_refreshed_successfuly {
    return Intl.message(
      'Notificaciones actualizadas con éxito',
      name: 'notifications_refreshed_successfuly',
      desc: '',
      args: [],
    );
  }

  /// `Pedido actualizado correctamente`
  String get order_refreshed_successfuly {
    return Intl.message(
      'Pedido actualizado correctamente',
      name: 'order_refreshed_successfuly',
      desc: '',
      args: [],
    );
  }

  /// `Pedidos actualizados correctamente`
  String get orders_refreshed_successfuly {
    return Intl.message(
      'Pedidos actualizados correctamente',
      name: 'orders_refreshed_successfuly',
      desc: '',
      args: [],
    );
  }

  /// `Productos actualizados con éxito.`
  String get market_refreshed_successfuly {
    return Intl.message(
      'Productos actualizados con éxito.',
      name: 'market_refreshed_successfuly',
      desc: '',
      args: [],
    );
  }

  /// `La configuración del perfil se actualizó correctamente`
  String get profile_settings_updated_successfully {
    return Intl.message(
      'La configuración del perfil se actualizó correctamente',
      name: 'profile_settings_updated_successfully',
      desc: '',
      args: [],
    );
  }

  /// `Configuración de pago actualizada correctamente`
  String get payment_settings_updated_successfully {
    return Intl.message(
      'Configuración de pago actualizada correctamente',
      name: 'payment_settings_updated_successfully',
      desc: '',
      args: [],
    );
  }

  /// `Seguimiento actualizado con éxito`
  String get tracking_refreshed_successfuly {
    return Intl.message(
      'Seguimiento actualizado con éxito',
      name: 'tracking_refreshed_successfuly',
      desc: '',
      args: [],
    );
  }

  /// `Bienvenidos`
  String get welcome {
    return Intl.message(
      'Bienvenidos',
      name: 'welcome',
      desc: '',
      args: [],
    );
  }

  /// `Correo o contraseña equivocada`
  String get wrong_email_or_password {
    return Intl.message(
      'Correo o contraseña equivocada',
      name: 'wrong_email_or_password',
      desc: '',
      args: [],
    );
  }

  /// `Direcciones actualizadas con éxito`
  String get addresses_refreshed_successfuly {
    return Intl.message(
      'Direcciones actualizadas con éxito',
      name: 'addresses_refreshed_successfuly',
      desc: '',
      args: [],
    );
  }

  /// `Direcciones de entrega`
  String get delivery_addresses {
    return Intl.message(
      'Direcciones de entrega',
      name: 'delivery_addresses',
      desc: '',
      args: [],
    );
  }

  /// `Añadir`
  String get add {
    return Intl.message(
      'Añadir',
      name: 'add',
      desc: '',
      args: [],
    );
  }

  /// `Nueva dirección agregada exitosamente`
  String get new_address_added_successfully {
    return Intl.message(
      'Nueva dirección agregada exitosamente',
      name: 'new_address_added_successfully',
      desc: '',
      args: [],
    );
  }

  /// `La dirección se actualizó correctamente`
  String get the_address_updated_successfully {
    return Intl.message(
      'La dirección se actualizó correctamente',
      name: 'the_address_updated_successfully',
      desc: '',
      args: [],
    );
  }

  /// `Mantenga presionado para editar el elemento, deslice el elemento para eliminarlo`
  String get long_press_to_edit_item_swipe_item_to_delete_it {
    return Intl.message(
      'Mantenga presionado para editar el elemento, deslice el elemento para eliminarlo',
      name: 'long_press_to_edit_item_swipe_item_to_delete_it',
      desc: '',
      args: [],
    );
  }

  /// `Agregar dirección de entrega`
  String get add_delivery_address {
    return Intl.message(
      'Agregar dirección de entrega',
      name: 'add_delivery_address',
      desc: '',
      args: [],
    );
  }

  /// `Dirección de casa`
  String get home_address {
    return Intl.message(
      'Dirección de casa',
      name: 'home_address',
      desc: '',
      args: [],
    );
  }

  /// `Descripción`
  String get description {
    return Intl.message(
      'Descripción',
      name: 'description',
      desc: '',
      args: [],
    );
  }

  /// `12 Street, City 21663, País`
  String get hint_full_address {
    return Intl.message(
      '12 Street, City 21663, País',
      name: 'hint_full_address',
      desc: '',
      args: [],
    );
  }

  /// `Dirección completa`
  String get full_address {
    return Intl.message(
      'Dirección completa',
      name: 'full_address',
      desc: '',
      args: [],
    );
  }

  /// `Pedidos`
  String get orders {
    return Intl.message(
      'Pedidos',
      name: 'orders',
      desc: '',
      args: [],
    );
  }

  /// `Historia`
  String get history {
    return Intl.message(
      'Historia',
      name: 'history',
      desc: '',
      args: [],
    );
  }

  /// `Entregado`
  String get delivered {
    return Intl.message(
      'Entregado',
      name: 'delivered',
      desc: '',
      args: [],
    );
  }

  /// `Descartar`
  String get dismiss {
    return Intl.message(
      'Descartar',
      name: 'dismiss',
      desc: '',
      args: [],
    );
  }

  /// `Confirmar`
  String get confirm {
    return Intl.message(
      'Confirmar',
      name: 'confirm',
      desc: '',
      args: [],
    );
  }

  /// `¿Podría confirmar si ha entregado todas las comidas al cliente?`
  String get would_you_please_confirm_if_you_have_delivered_all_meals {
    return Intl.message(
      '¿Podría confirmar si ha entregado todas las comidas al cliente?',
      name: 'would_you_please_confirm_if_you_have_delivered_all_meals',
      desc: '',
      args: [],
    );
  }

  /// `Confirmacion de envio`
  String get delivery_confirmation {
    return Intl.message(
      'Confirmacion de envio',
      name: 'delivery_confirmation',
      desc: '',
      args: [],
    );
  }

  /// `Alimentos ordenados`
  String get products_ordered {
    return Intl.message(
      'Alimentos ordenados',
      name: 'products_ordered',
      desc: '',
      args: [],
    );
  }

  /// `Detalles del pedido`
  String get order_details {
    return Intl.message(
      'Detalles del pedido',
      name: 'order_details',
      desc: '',
      args: [],
    );
  }

  /// `Dirección no proporcionada por favor llame al cliente`
  String get address_not_provided_please_call_the_client {
    return Intl.message(
      'Dirección no proporcionada por favor llame al cliente',
      name: 'address_not_provided_please_call_the_client',
      desc: '',
      args: [],
    );
  }

  /// `Dirección no proporcionada contacto cliente`
  String get address_not_provided_contact_client {
    return Intl.message(
      'Dirección no proporcionada contacto cliente',
      name: 'address_not_provided_contact_client',
      desc: '',
      args: [],
    );
  }

  /// `Historial de pedidos`
  String get orders_history {
    return Intl.message(
      'Historial de pedidos',
      name: 'orders_history',
      desc: '',
      args: [],
    );
  }

  /// `Correo electrónico para restablecer contraseña`
  String get email_to_reset_password {
    return Intl.message(
      'Correo electrónico para restablecer contraseña',
      name: 'email_to_reset_password',
      desc: '',
      args: [],
    );
  }

  /// `Enviar enlace`
  String get send_password_reset_link {
    return Intl.message(
      'Enviar enlace',
      name: 'send_password_reset_link',
      desc: '',
      args: [],
    );
  }

  /// `Recuerdo mi contraseña volver al inicio de sesión`
  String get i_remember_my_password_return_to_login {
    return Intl.message(
      'Recuerdo mi contraseña volver al inicio de sesión',
      name: 'i_remember_my_password_return_to_login',
      desc: '',
      args: [],
    );
  }

  /// `Su enlace de reinicio ha sido enviado a su correo electrónico`
  String get your_reset_link_has_been_sent_to_your_email {
    return Intl.message(
      'Su enlace de reinicio ha sido enviado a su correo electrónico',
      name: 'your_reset_link_has_been_sent_to_your_email',
      desc: '',
      args: [],
    );
  }

  /// `¡Error! Verificar la configuración del correo electrónico`
  String get error_verify_email_settings {
    return Intl.message(
      '¡Error! Verificar la configuración del correo electrónico',
      name: 'error_verify_email_settings',
      desc: '',
      args: [],
    );
  }

  /// `Estado del pedido cambiado`
  String get order_satatus_changed {
    return Intl.message(
      'Estado del pedido cambiado',
      name: 'order_satatus_changed',
      desc: '',
      args: [],
    );
  }

  /// `Nuevo pedido del cliente`
  String get new_order_from_costumer {
    return Intl.message(
      'Nuevo pedido del cliente',
      name: 'new_order_from_costumer',
      desc: '',
      args: [],
    );
  }

  /// `Tienes un pedido asignado`
  String get your_have_an_order_assigned_to_you {
    return Intl.message(
      'Tienes un pedido asignado',
      name: 'your_have_an_order_assigned_to_you',
      desc: '',
      args: [],
    );
  }

  /// `Desconocido`
  String get unknown {
    return Intl.message(
      'Desconocido',
      name: 'unknown',
      desc: '',
      args: [],
    );
  }

  /// `Alimentos ordenados`
  String get ordered_products {
    return Intl.message(
      'Alimentos ordenados',
      name: 'ordered_products',
      desc: '',
      args: [],
    );
  }

  /// `Gastos de envío`
  String get delivery_fee {
    return Intl.message(
      'Gastos de envío',
      name: 'delivery_fee',
      desc: '',
      args: [],
    );
  }

  /// `Artículos`
  String get items {
    return Intl.message(
      'Artículos',
      name: 'items',
      desc: '',
      args: [],
    );
  }

  /// `¡No tienes ningún pedido asignado!`
  String get you_dont_have_any_order_assigned_to_you {
    return Intl.message(
      '¡No tienes ningún pedido asignado!',
      name: 'you_dont_have_any_order_assigned_to_you',
      desc: '',
      args: [],
    );
  }

  /// `Deslice hacia la izquierda la notificación para borrarla o leerla / no leerla`
  String get swip_left_the_notification_to_delete_or_read__unread {
    return Intl.message(
      'Deslice hacia la izquierda la notificación para borrarla o leerla / no leerla',
      name: 'swip_left_the_notification_to_delete_or_read__unread',
      desc: '',
      args: [],
    );
  }

  /// `Cliente`
  String get customer {
    return Intl.message(
      'Cliente',
      name: 'customer',
      desc: '',
      args: [],
    );
  }

  /// `Km`
  String get km {
    return Intl.message(
      'Km',
      name: 'km',
      desc: '',
      args: [],
    );
  }

  /// `mi`
  String get mi {
    return Intl.message(
      'mi',
      name: 'mi',
      desc: '',
      args: [],
    );
  }

  /// `Cantidad`
  String get quantity {
    return Intl.message(
      'Cantidad',
      name: 'quantity',
      desc: '',
      args: [],
    );
  }

  /// `Esta cuenta no existe.`
  String get thisAccountNotExist {
    return Intl.message(
      'Esta cuenta no existe.',
      name: 'thisAccountNotExist',
      desc: '',
      args: [],
    );
  }

  /// `Vuelve a presionar para salir`
  String get tapBackAgainToLeave {
    return Intl.message(
      'Vuelve a presionar para salir',
      name: 'tapBackAgainToLeave',
      desc: '',
      args: [],
    );
  }

  /// `Número de Teléfono`
  String get phoneNumber {
    return Intl.message(
      'Número de Teléfono',
      name: 'phoneNumber',
      desc: '',
      args: [],
    );
  }

  /// `Dirección de Entrega`
  String get deliveryAddress {
    return Intl.message(
      'Dirección de Entrega',
      name: 'deliveryAddress',
      desc: '',
      args: [],
    );
  }

  /// `Nombre Completo`
  String get fullName {
    return Intl.message(
      'Nombre Completo',
      name: 'fullName',
      desc: '',
      args: [],
    );
  }

  /// `Ver Detalles`
  String get viewDetails {
    return Intl.message(
      'Ver Detalles',
      name: 'viewDetails',
      desc: '',
      args: [],
    );
  }

  /// `Todos`
  String get all {
    return Intl.message(
      'Todos',
      name: 'all',
      desc: '',
      args: [],
    );
  }

  /// `Ganancia Total`
  String get totalEarning {
    return Intl.message(
      'Ganancia Total',
      name: 'totalEarning',
      desc: '',
      args: [],
    );
  }

  /// `Total de Órdenes`
  String get totalOrders {
    return Intl.message(
      'Total de Órdenes',
      name: 'totalOrders',
      desc: '',
      args: [],
    );
  }

  /// `Total de Mercados`
  String get totalMarkets {
    return Intl.message(
      'Total de Mercados',
      name: 'totalMarkets',
      desc: '',
      args: [],
    );
  }

  /// `Total de Productos`
  String get totalProducts {
    return Intl.message(
      'Total de Productos',
      name: 'totalProducts',
      desc: '',
      args: [],
    );
  }

  /// `Mis Mercados`
  String get myMarkets {
    return Intl.message(
      'Mis Mercados',
      name: 'myMarkets',
      desc: '',
      args: [],
    );
  }

  /// `Cerrado`
  String get closed {
    return Intl.message(
      'Cerrado',
      name: 'closed',
      desc: '',
      args: [],
    );
  }

  /// `Abierto`
  String get open {
    return Intl.message(
      'Abierto',
      name: 'open',
      desc: '',
      args: [],
    );
  }

  /// `Entrega`
  String get delivery {
    return Intl.message(
      'Entrega',
      name: 'delivery',
      desc: '',
      args: [],
    );
  }

  /// `Recoger`
  String get pickup {
    return Intl.message(
      'Recoger',
      name: 'pickup',
      desc: '',
      args: [],
    );
  }

  /// `Información`
  String get information {
    return Intl.message(
      'Información',
      name: 'information',
      desc: '',
      args: [],
    );
  }

  /// `Productos`
  String get featuredProducts {
    return Intl.message(
      'Productos',
      name: 'featuredProducts',
      desc: '',
      args: [],
    );
  }

  /// `¿Lo que dicen ellos?`
  String get whatTheySay {
    return Intl.message(
      '¿Lo que dicen ellos?',
      name: 'whatTheySay',
      desc: '',
      args: [],
    );
  }

  /// `Ver`
  String get view {
    return Intl.message(
      'Ver',
      name: 'view',
      desc: '',
      args: [],
    );
  }

  /// `Confirmación`
  String get confirmation {
    return Intl.message(
      'Confirmación',
      name: 'confirmation',
      desc: '',
      args: [],
    );
  }

  /// `Si`
  String get yes {
    return Intl.message(
      'Si',
      name: 'yes',
      desc: '',
      args: [],
    );
  }

  /// `¿Está seguro que desea cancelar este pedido de cliente?`
  String get areYouSureYouWantToCancelThisOrderOf {
    return Intl.message(
      '¿Está seguro que desea cancelar este pedido de cliente?',
      name: 'areYouSureYouWantToCancelThisOrderOf',
      desc: '',
      args: [],
    );
  }

  /// `Editar Orden`
  String get editOrder {
    return Intl.message(
      'Editar Orden',
      name: 'editOrder',
      desc: '',
      args: [],
    );
  }

  /// `Guardar Cambios`
  String get saveChanges {
    return Intl.message(
      'Guardar Cambios',
      name: 'saveChanges',
      desc: '',
      args: [],
    );
  }

  /// `Estado de Orden`
  String get orderStatus {
    return Intl.message(
      'Estado de Orden',
      name: 'orderStatus',
      desc: '',
      args: [],
    );
  }

  /// `Se actualizó correctamente la orden`
  String get thisOrderUpdatedSuccessfully {
    return Intl.message(
      'Se actualizó correctamente la orden',
      name: 'thisOrderUpdatedSuccessfully',
      desc: '',
      args: [],
    );
  }

  /// `Asignar repartidor`
  String get assignDeliveryBoy {
    return Intl.message(
      'Asignar repartidor',
      name: 'assignDeliveryBoy',
      desc: '',
      args: [],
    );
  }

  /// `Información General`
  String get generalInformation {
    return Intl.message(
      'Información General',
      name: 'generalInformation',
      desc: '',
      args: [],
    );
  }

  /// `Pista`
  String get hint {
    return Intl.message(
      'Pista',
      name: 'hint',
      desc: '',
      args: [],
    );
  }

  /// `Ingresar información general de la orden`
  String get insertAnAdditionalInformationForThisOrder {
    return Intl.message(
      'Ingresar información general de la orden',
      name: 'insertAnAdditionalInformationForThisOrder',
      desc: '',
      args: [],
    );
  }

  /// `Orden: #{id} fue cancelada`
  String orderIdHasBeenCanceled(Object id) {
    return Intl.message(
      'Orden: #$id fue cancelada',
      name: 'orderIdHasBeenCanceled',
      desc: '',
      args: [id],
    );
  }

  /// `Cancelado`
  String get canceled {
    return Intl.message(
      'Cancelado',
      name: 'canceled',
      desc: '',
      args: [],
    );
  }

  /// `Mensajes`
  String get messages {
    return Intl.message(
      'Mensajes',
      name: 'messages',
      desc: '',
      args: [],
    );
  }

  /// `No tienes ninguna conversación`
  String get youDontHaveAnyConversations {
    return Intl.message(
      'No tienes ninguna conversación',
      name: 'youDontHaveAnyConversations',
      desc: '',
      args: [],
    );
  }

  /// `Nuevo mensaje de`
  String get newMessageFrom {
    return Intl.message(
      'Nuevo mensaje de',
      name: 'newMessageFrom',
      desc: '',
      args: [],
    );
  }

  /// `Escribe para empezar un chat`
  String get typeToStartChat {
    return Intl.message(
      'Escribe para empezar un chat',
      name: 'typeToStartChat',
      desc: '',
      args: [],
    );
  }

  /// `Notificación marcada como leída`
  String get thisNotificationHasMarkedAsRead {
    return Intl.message(
      'Notificación marcada como leída',
      name: 'thisNotificationHasMarkedAsRead',
      desc: '',
      args: [],
    );
  }

  /// `Notificación marcada como no leída`
  String get thisNotificationHasMarkedAsUnRead {
    return Intl.message(
      'Notificación marcada como no leída',
      name: 'thisNotificationHasMarkedAsUnRead',
      desc: '',
      args: [],
    );
  }

  /// `Notificación eliminada`
  String get notificationWasRemoved {
    return Intl.message(
      'Notificación eliminada',
      name: 'notificationWasRemoved',
      desc: '',
      args: [],
    );
  }

  /// `Mis Productos`
  String get myProducts {
    return Intl.message(
      'Mis Productos',
      name: 'myProducts',
      desc: '',
      args: [],
    );
  }

  /// `No tienes ningún producto asociado a tu cuenta.`
  String get noProductsUser {
    return Intl.message(
      'No tienes ningún producto asociado a tu cuenta.',
      name: 'noProductsUser',
      desc: '',
      args: [],
    );
  }

  /// `Crear`
  String get create {
    return Intl.message(
      'Crear',
      name: 'create',
      desc: '',
      args: [],
    );
  }

  /// `Crear un producto nuevo`
  String get createProduct {
    return Intl.message(
      'Crear un producto nuevo',
      name: 'createProduct',
      desc: '',
      args: [],
    );
  }

  /// `Nombre del producto`
  String get productName {
    return Intl.message(
      'Nombre del producto',
      name: 'productName',
      desc: '',
      args: [],
    );
  }

  /// `Producto`
  String get productNameExample {
    return Intl.message(
      'Producto',
      name: 'productNameExample',
      desc: '',
      args: [],
    );
  }

  /// `Precio del Producto`
  String get productPrice {
    return Intl.message(
      'Precio del Producto',
      name: 'productPrice',
      desc: '',
      args: [],
    );
  }

  /// `Precio de Descuento`
  String get productDiscount {
    return Intl.message(
      'Precio de Descuento',
      name: 'productDiscount',
      desc: '',
      args: [],
    );
  }

  /// `Capacidad`
  String get capacity {
    return Intl.message(
      'Capacidad',
      name: 'capacity',
      desc: '',
      args: [],
    );
  }

  /// `Elíge una foto para el producto`
  String get choosePhoto {
    return Intl.message(
      'Elíge una foto para el producto',
      name: 'choosePhoto',
      desc: '',
      args: [],
    );
  }

  /// `Cámara`
  String get camera {
    return Intl.message(
      'Cámara',
      name: 'camera',
      desc: '',
      args: [],
    );
  }

  /// `Galeria`
  String get gallery {
    return Intl.message(
      'Galeria',
      name: 'gallery',
      desc: '',
      args: [],
    );
  }

  /// `Producto creado correctamente`
  String get productAdded {
    return Intl.message(
      'Producto creado correctamente',
      name: 'productAdded',
      desc: '',
      args: [],
    );
  }

  /// `No se pudo crear producto`
  String get productNotAdded {
    return Intl.message(
      'No se pudo crear producto',
      name: 'productNotAdded',
      desc: '',
      args: [],
    );
  }

  /// `Descripción`
  String get productDescription {
    return Intl.message(
      'Descripción',
      name: 'productDescription',
      desc: '',
      args: [],
    );
  }

  /// `Ingredientes`
  String get productIngredients {
    return Intl.message(
      'Ingredientes',
      name: 'productIngredients',
      desc: '',
      args: [],
    );
  }

  /// `Capacidad`
  String get productCapacity {
    return Intl.message(
      'Capacidad',
      name: 'productCapacity',
      desc: '',
      args: [],
    );
  }

  /// `Unidad`
  String get productUnit {
    return Intl.message(
      'Unidad',
      name: 'productUnit',
      desc: '',
      args: [],
    );
  }

  /// `Cantidad de objetos en empaque`
  String get productPackageItems {
    return Intl.message(
      'Cantidad de objetos en empaque',
      name: 'productPackageItems',
      desc: '',
      args: [],
    );
  }

  /// `Disponible en servicio a domicilio`
  String get productDeliverable {
    return Intl.message(
      'Disponible en servicio a domicilio',
      name: 'productDeliverable',
      desc: '',
      args: [],
    );
  }

  /// `Nombre de producto no válido`
  String get productNameNotValid {
    return Intl.message(
      'Nombre de producto no válido',
      name: 'productNameNotValid',
      desc: '',
      args: [],
    );
  }

  /// `Precio de producto no válido`
  String get productPriceNotValid {
    return Intl.message(
      'Precio de producto no válido',
      name: 'productPriceNotValid',
      desc: '',
      args: [],
    );
  }

  /// `Servicio a domicilio`
  String get deliverable {
    return Intl.message(
      'Servicio a domicilio',
      name: 'deliverable',
      desc: '',
      args: [],
    );
  }

  /// `Sin servicio a domicilio`
  String get not_deliverable {
    return Intl.message(
      'Sin servicio a domicilio',
      name: 'not_deliverable',
      desc: '',
      args: [],
    );
  }

  /// `Comentarios`
  String get reviews {
    return Intl.message(
      'Comentarios',
      name: 'reviews',
      desc: '',
      args: [],
    );
  }

  /// `Opciones`
  String get options {
    return Intl.message(
      'Opciones',
      name: 'options',
      desc: '',
      args: [],
    );
  }

  /// `Disponible`
  String get available {
    return Intl.message(
      'Disponible',
      name: 'available',
      desc: '',
      args: [],
    );
  }

  /// `No Disponible`
  String get not_available {
    return Intl.message(
      'No Disponible',
      name: 'not_available',
      desc: '',
      args: [],
    );
  }

  /// `Edita el producto`
  String get edit_product {
    return Intl.message(
      'Edita el producto',
      name: 'edit_product',
      desc: '',
      args: [],
    );
  }

  /// `Producto se añadió a favoritos`
  String get added_to_favorite {
    return Intl.message(
      'Producto se añadió a favoritos',
      name: 'added_to_favorite',
      desc: '',
      args: [],
    );
  }

  /// `Producto editado correctamente`
  String get product_edited {
    return Intl.message(
      'Producto editado correctamente',
      name: 'product_edited',
      desc: '',
      args: [],
    );
  }

  /// `Producto eliminado de favoritos`
  String get deleted_to_favorite {
    return Intl.message(
      'Producto eliminado de favoritos',
      name: 'deleted_to_favorite',
      desc: '',
      args: [],
    );
  }

  /// `Se actualizó el producto correctamente`
  String get product_refreshed {
    return Intl.message(
      'Se actualizó el producto correctamente',
      name: 'product_refreshed',
      desc: '',
      args: [],
    );
  }

  /// `Debes seleccionar una categoría`
  String get select_category {
    return Intl.message(
      'Debes seleccionar una categoría',
      name: 'select_category',
      desc: '',
      args: [],
    );
  }

  /// `Eliminar`
  String get delete {
    return Intl.message(
      'Eliminar',
      name: 'delete',
      desc: '',
      args: [],
    );
  }

  /// `Producto eliminado correctamente`
  String get deleted_product {
    return Intl.message(
      'Producto eliminado correctamente',
      name: 'deleted_product',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'es'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}